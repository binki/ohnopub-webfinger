A very simple [WebFinger](https://tools.ietf.org/html/rfc7033)
implementation meant to point to an OpenID Connect instance as
configured in config.json.


The goal of this WebFinger implementation is to support a
standards-based mapping of email addresses onto identity providers for
Portier (formerly Letsauth). It does this by implementing the Host
role described in section 2 of [OpenID Connect Discovery
1.0](http://openid.net/specs/openid-connect-discovery-1_0.html#IssuerDiscovery)
to support lookups based on `acct:` and `mailto:` URIs. Because
Portier uses emails as its basis for identity, there is no need to
support mapping an arbitrary URI onto a user as Portier will only ever
make `acct:` or `mailto:` lookups.

# Resource Schema

Section 2.1.2 dictates that something that looks like an email should
use the [`acct:` scheme](https://tools.ietf.org/html/rfc7565) scheme
when performing lookup. This RFC includes the following:

> However, there exists no URI scheme that generically
> identifies a user's account at a service provider without specifying
> a particular protocol to use when interacting with the account.  This
> specification fills that gap.

and

> not all service providers offer email services or web
> interfaces on behalf of user accounts

However, because Portier is based on email addresses, Portier will use
the `mailto:` scheme when looking up accounts. If Portier looked up the
entity using the `acct:` URI it cannot assume that the endpoint has an
email address. It is valid for a WebFinger service to indicate that a
`mailto:` URI maps onto a particular `acct:` by replying with the
canonical URI for the resource. However, Portier will always use
`mailto:` when doing lookups because Portier is fundamentally email
bound. Poriter might have to send a login link via email if idP lookup
fails or if the idP itself is failing, for example.

In fact, [section 4 of RFC
7565](https://tools.ietf.org/html/rfc7565#section-4) recommends
against treating an `acct:` URI as transformable into an email address
and recommends to lookup via `mailto:` if looking up an email (rather
than account) is required.
