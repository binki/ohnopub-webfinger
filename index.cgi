#!/usr/bin/env node
/* -*- mode: js2; indent-tabs-mode: nil; -*- */
'use strict';

const autoserve = require('autoserve');
const url = require('url');

const config = require('./config.json');

autoserve(function (request, response) {
    Promise.resolve().then(function () {
        var badRequest = function (code, message) {
            response.writeHead(code, { 'Content-Type': 'text/plain; charset=utf-8', });
            response.end(`Improper request: ${message}\n`);
        };

        if (request.method !== 'GET') {
            return badRequest(405, `Method “${request.method}” unsupported.`);
        }

        var parsedUrl = url.parse(request.url, true);
        if (!parsedUrl.query.resource) {
            return badRequest(400, 'Missing parameter resource.');
        }
        var subject = url.parse(parsedUrl.query.resource, true);
        /* Canonization. */
        if (subject.protocol === 'mailto:') {
            if (!subject.auth) {
                return badRequest(400, `username missing from email address.`);
            }
            if (!subject.host) {
                return badRequest(400, `host missing from email address`);
            }
            if (subject.host !== request.headers.host) {
                /*
                 * Be careful here. If the specified subject is of a
                 * foreign email address, claiming that this address
                 * belongs to your own system could compromise
                 * security because you might inadvertently claim that
                 * an email address whose authentication you may have
                 * no control over should be trusted as much as an
                 * email address whose authentication you’d expect to
                 * control.
                 */
                return badRequest(400, `specified email’s authority ${subject.host} does not match this server’s host ${request.headers.host}`);
            }
            subject = url.parse(url.format({
                protocol: 'acct:',
                /*
                 * Here you would need to insert any special
                 * canonization rules of your services such as
                 * stripping “+tag” out of email addresses.
                 */
                auth: subject.auth,
                hostname: subject.hostname,
            }));
        }

        if (subject.protocol === 'acct:') {
            /* Strip password out of username portion. */
            subject.auth = subject.auth.split(':')[0];
        }

        /*
         * Build aliases.
         */
        var aliases = [];
        if (subject.protocol === 'acct:') {
            /*
             * For our purposes, we are defining an account as being
             * equivalent to an email address. So list the email
             * address here. Unfortunately this won’t support “+tag”
             * emails properly since those are not able to be looked
             * up.
             */
            aliases.push(url.format({
                protocol: 'mailto:',
                auth: subject.auth,
                hostname: subject.hostname,
            }));
        }

        /* rel may be singleton or multiple or none. */
        var requestedRels = [].concat(parsedUrl.query.rel).filter(x => x);
        if (!requestedRels.length) {
            requestedRels = null;
        }
        var links = [];
        var addIf = function (rel, builder) {
            if (!requestedRels || requestedRels.indexOf(rel) > -1) {
                links.push(Object.assign({ rel: rel, }, builder()));
            }
        };
        if (subject.protocol === 'acct:') {
            addIf('http://openid.net/specs/connect/1.0/issuer', () => ({ href: config.openidUrl, }));
        }
        response.writeHead(
            200,
            {
                'Content-Type': 'application/jrd+json',
                /* https://tools.ietf.org/html/rfc7033#section-5 */
                'Access-Control-Allow-Origin': '*',
            });
        response.end(JSON.stringify({
            subject: subject.href,
            aliases: aliases,
            links: links,
        }));
    }).then(null, function (e) {
        response.writeHead(501, { 'Content-Type': 'text/plain; charset=utf-8', });
        response.end(`Sorry: ${e.message}\n`);
    });
});
